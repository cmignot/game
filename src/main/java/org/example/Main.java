package org.example;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        var game = new Game();
        var scanner = new Scanner(System.in);
        var random = new Random();


        System.out.println("Enter player " + (game.howManyPlayers()+1) + " name");
        game.add(scanner.nextLine());
        for (var anotherPlayer = true ; anotherPlayer || !game.isPlayable() ;) {
            System.out.println("Enter player " + (game.howManyPlayers()+1) + " name");
            game.add(scanner.nextLine());
            System.out.println("Is there another player ? (Y/N)");
            anotherPlayer = isUserSayingYes(scanner.nextLine());
        }

        var gameWon = false;
        do {
            game.roll(random.nextInt(1, 7));
            if (game.expectedAnswer()) {
                System.out.println("When you think you have the answer, press enter.");
                scanner.nextLine();
                game.printCurrentAnswer();
                System.out.println("Did you have the right answer ? (Y/N)");
                if (isUserSayingYes(scanner.nextLine())) {
                    gameWon = game.wasCorrectlyAnswered();
                } else {
                    game.wrongAnswer();
                }
            }

        } while (!gameWon);
    }

    private static boolean isUserSayingYes(String userAnswer) {
        var answer = userAnswer.toLowerCase();
        return "y".equals(answer) || "yes".equals(answer);
    }

}
