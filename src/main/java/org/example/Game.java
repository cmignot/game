package org.example;

import com.opencsv.CSVReader;
import org.apache.commons.lang3.Validate;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import static org.example.Category.*;

public class Game {
	private final ArrayList<String> players = new ArrayList<>();
    private final int[] places = new int[6];
	private final int[] purses = new int[6];
	private final boolean[] inPenaltyBox  = new boolean[6];

	private final LinkedList<String[]> popQuestions = new LinkedList<>();
	private final LinkedList<String[]> scienceQuestions = new LinkedList<>();
	private final LinkedList<String[]> sportsQuestions = new LinkedList<>();
	private final LinkedList<String[]> rockQuestions = new LinkedList<>();
	private final Random random = new Random();
    
    private int currentPlayer = 0;
	private boolean isGettingOutOfPenaltyBox;
	private boolean expectedAnswer = false;
	private String currentAnswer;

    public Game() {
		popQuestions.addAll(loadQuestions("pop.csv"));
		scienceQuestions.addAll(loadQuestions("science.csv"));
		sportsQuestions.addAll(loadQuestions("sports.csv"));
		rockQuestions.addAll(loadQuestions("rock.csv"));
	}

	private static List<String[]> loadQuestions(String filePath) {
		try {
			var path = Paths.get(ClassLoader.getSystemResource(filePath).toURI());
			return new CSVReader(Files.newBufferedReader(path)).readAll();
		} catch (URISyntaxException | IOException e) {
			throw new RuntimeException(e);
		}
	}

    public boolean expectedAnswer() {
        return expectedAnswer;
    }

	public boolean isPlayable() {
		return howManyPlayers() >= 2;
	}

	public boolean add(String playerName) {
		if (players.size() == 6) return false;

		Validate.notNull(playerName, "playerName must not be null");
		Validate.notBlank(playerName, "playerName must not be blank");

	    players.add(playerName);
		int playerIndex = howManyPlayers() - 1;
		places[playerIndex] = 0;
	    purses[playerIndex] = 0;
	    inPenaltyBox[playerIndex] = false;
	    
	    System.out.println(playerName + " was added");
	    System.out.println("They are player number " + players.size());
		return true;
	}
	
	public int howManyPlayers() {
		return players.size();
	}

	public void roll(int roll) {
		if (expectedAnswer) {
			throw new IllegalStateException("The player did not answer the question.");
		}

		System.out.println(players.get(currentPlayer) + " is the current player");
		System.out.println("They have rolled a " + roll);
		
		if (inPenaltyBox[currentPlayer]) {
			if (roll % 2 != 0) {
				isGettingOutOfPenaltyBox = true;
				System.out.println(players.get(currentPlayer) + " is getting out of the penalty box");

				moveToNewPlace(roll);
			} else {
				System.out.println(players.get(currentPlayer) + " is not getting out of the penalty box");
				isGettingOutOfPenaltyBox = false;
				nextPlayer();
			}
			
		} else {
			moveToNewPlace(roll);
		}
		
	}

	public void printCurrentAnswer() {
		System.out.println("The correct answer was: " + currentAnswer);
	}

	private void moveToNewPlace(int roll) {
		places[currentPlayer] = places[currentPlayer] + roll;
		if (places[currentPlayer] > 11) places[currentPlayer] = places[currentPlayer] - 12;

		System.out.println(players.get(currentPlayer) 
				+ "'s new location is " 
				+ places[currentPlayer]);
		System.out.println("The category is " + currentCategory().value());
		askQuestion();
	}

	private void askQuestion() {
		switch (currentCategory()) {
			case POP -> {
				String[] questionAndAnswer = popQuestions.get(random.nextInt(0, popQuestions.size()));
				System.out.println(questionAndAnswer[0]);
				currentAnswer = questionAndAnswer[1];
			}
			case SCIENCE -> {
				String[] questionAndAnswer = scienceQuestions.get(random.nextInt(0, scienceQuestions.size()));
				System.out.println(questionAndAnswer[0]);
				currentAnswer = questionAndAnswer[1];
			}
			case SPORTS -> {
				String[] questionAndAnswer = sportsQuestions.get(random.nextInt(0, sportsQuestions.size()));
				System.out.println(questionAndAnswer[0]);
				currentAnswer = questionAndAnswer[1];
			}
			case ROCK -> {
				String[] questionAndAnswer = rockQuestions.get(random.nextInt(0, rockQuestions.size()));
				System.out.println(questionAndAnswer[0]);
				currentAnswer = questionAndAnswer[1];
			}
		}
		expectedAnswer = true;
	}
	
	
	private Category currentCategory() {
		return switch(places[currentPlayer]) {
			case 0, 4, 8 -> POP;
			case 1, 5, 9 -> SCIENCE;
			case 2, 6, 10 -> SPORTS;
			default -> ROCK;
		};
	}

	public boolean wasCorrectlyAnswered() {
		if (!expectedAnswer) {
			throw new IllegalStateException("No questions were asked.");
		}

		if (inPenaltyBox[currentPlayer]) {
			if (isGettingOutOfPenaltyBox) {
				inPenaltyBox[currentPlayer] = false;
				return correctAnswer();
			} else {
				nextPlayer();
				return false;
			}
		} else {
			return correctAnswer();
		}
	}

	private boolean correctAnswer() {
		System.out.println("Answer was correct!!!!");
		purses[currentPlayer]++;
		System.out.println(players.get(currentPlayer)
				+ " now has "
				+ purses[currentPlayer]
				+ " Gold Coins.");

		boolean winner = didPlayerWin();
		if (winner) {
			System.out.println(players.get(currentPlayer)+" won the game!!");
		}

		nextPlayer();
		return winner;
	}

	public void wrongAnswer(){
		if (!expectedAnswer) {
			throw new IllegalStateException("No questions were asked.");
		}

		System.out.println("Question was incorrectly answered");
		System.out.println(players.get(currentPlayer)+ " was sent to the penalty box");
		inPenaltyBox[currentPlayer] = true;

		nextPlayer();
	}

	private void nextPlayer() {
		currentPlayer++;
		if (currentPlayer == players.size()) currentPlayer = 0;
		expectedAnswer = false;
		currentAnswer = null;
	}

	private boolean didPlayerWin() {
		return purses[currentPlayer] == 6;
	}
}
