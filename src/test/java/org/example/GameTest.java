package org.example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class GameTest {

    @Test
    void gameWonWhenPlayerHasSixGoldCoins() {
        var game = new Game();
        game.add("Player 1");
        game.add("Player 2");

        /* Player 1 turn: answers correctly and earns a coin.
        * Player 1: 1 gold coin
        * Player 2: 0 gold coin
        */
        game.roll(6);
        Assertions.assertFalse(game.wasCorrectlyAnswered());

        /* Player 2 turn: answers correctly and earns a coin.
        * Player 1: 1 gold coin
        * Player 2: 1 gold coin
        */
        game.roll(1);
        Assertions.assertFalse(game.wasCorrectlyAnswered());

        /* Player 1 turn: answers correctly and earns a coin.
         * Player 1: 2 gold coins
         * Player 2: 1 gold coin
         */
        game.roll(1);
        Assertions.assertFalse(game.wasCorrectlyAnswered());

        /* Player 2 turn: answers correctly and earns a coin.
         * Player 1: 2 gold coins
         * Player 2: 2 gold coins
         */
        game.roll(4);
        Assertions.assertFalse(game.wasCorrectlyAnswered());

        /* Player 1 turn: answers badly.
         * Player 1: 2 gold coins // penalty box
         * Player 2: 2 gold coins
         */
        game.roll(2);
        game.wrongAnswer();

        /* Player 2 turn: answers correctly and earns a coin.
         * Player 1: 2 gold coins // penalty box
         * Player 2: 3 gold coins
         */
        game.roll(4);
        Assertions.assertFalse(game.wasCorrectlyAnswered());

        /* Player 1 turn: is not getting out of the penalty box.
         * Player 1: 2 gold coins // penalty box
         * Player 2: 3 gold coins
         */
        game.roll(2);

        /* Player 2 turn: answers correctly and earns a coin.
         * Player 1: 2 gold coins // penalty box
         * Player 2: 4 gold coins
         */
        game.roll(3);
        Assertions.assertFalse(game.wasCorrectlyAnswered());

        /* Player 1 turn: is getting out of the penalty box and answers correctly.
         * Player 1: 3 gold coins
         * Player 2: 4 gold coins
         */
        game.roll(1);
        Assertions.assertFalse(game.wasCorrectlyAnswered());

        /* Player 2 turn: answers badly.
         * Player 1: 3 gold coins
         * Player 2: 4 gold coins // penalty box
         */
        game.roll(1);
        game.wrongAnswer();

        /* Player 1 turn: answers correctly and earns a coin.
         * Player 1: 4 gold coins
         * Player 2: 4 gold coins // penalty box
         */
        game.roll(1);
        Assertions.assertFalse(game.wasCorrectlyAnswered());

        /* Player 2 turn: is not getting out of the penalty box.
         * Player 1: 4 gold coins
         * Player 2: 4 gold coins // penalty box
         */
        game.roll(2);

        /* Player 1 turn: answers correctly and earns a coin.
         * Player 1: 5 gold coins
         * Player 2: 4 gold coins // penalty box
         */
        game.roll(5);
        Assertions.assertFalse(game.wasCorrectlyAnswered());

        /* Player 2 turn: is getting out of the penalty box and answers correctly.
         * Player 1: 5 gold coins
         * Player 2: 5 gold coins
         */
        game.roll(3);
        Assertions.assertFalse(game.wasCorrectlyAnswered());

        /* Player 1 turn: answers correctly, earns a coin and wins the game.
         * Player 1: 6 gold coins
         * Player 2: 5 gold coins
         */
        game.roll(2);
        Assertions.assertTrue(game.wasCorrectlyAnswered());
    }

    @Test
    void cannotRollIfExpectedAnswer() {
        var game = new Game();
        game.add("Player 1");
        game.add("Player 2");

        game.roll(2);
        Assertions.assertThrows(IllegalStateException.class, () -> game.roll(3));
    }

    @Test
    void cannotAnswerIfAnswerNotExpected() {
        var game = new Game();
        game.add("Player 1");
        game.add("Player 2");

        game.roll(2);
        game.wasCorrectlyAnswered();
        Assertions.assertThrows(IllegalStateException.class, game::wasCorrectlyAnswered);

        game.roll(1);
        game.wasCorrectlyAnswered();
        Assertions.assertThrows(IllegalStateException.class, game::wrongAnswer);

        game.roll(5);
        game.wrongAnswer();
        Assertions.assertThrows(IllegalStateException.class, game::wasCorrectlyAnswered);

        game.roll(3);
        game.wrongAnswer();
        Assertions.assertThrows(IllegalStateException.class, game::wrongAnswer);
    }

}