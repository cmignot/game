package org.example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.IntStream;
import java.util.stream.Stream;

class AddPlayerTest {

    private final Game game = new Game();

    @Test
    void addSixPlayersMax() {
        IntStream.range(0, 6).forEach(n -> Assertions.assertTrue(game.add("player "+n)));
        Assertions.assertFalse(game.add("unwanted player"));
    }

    private static Stream<Arguments> invalidPlayersNames() {
        return Stream.of(
                Arguments.of(null, NullPointerException.class, "playerName must not be null"),
                Arguments.of("", IllegalArgumentException.class, "playerName must not be blank"),
                Arguments.of("  ", IllegalArgumentException.class, "playerName must not be blank")
        );
    }

    @ParameterizedTest
    @MethodSource("invalidPlayersNames")
    void addPlayerWithInvalidName(String playerName, Class<Throwable> errorType, String expectedErrorMessage) {
        var exception = Assertions.assertThrows(errorType, () -> game.add(playerName));
        Assertions.assertEquals(exception.getMessage(), expectedErrorMessage);
    }

}