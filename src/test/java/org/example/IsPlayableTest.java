package org.example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.IntStream;
import java.util.stream.Stream;

class IsPlayableTest {

    private static Stream<Arguments> validPlayersNumber() {
        return Stream.of(
                Arguments.of(2),
                Arguments.of(3),
                Arguments.of(4),
                Arguments.of(5),
                Arguments.of(6)
        );
    }

    private static Stream<Arguments> invalidPlayersNumber() {
        return Stream.of(
                Arguments.of(0),
                Arguments.of(1)
        );
    }

    @ParameterizedTest
    @MethodSource("validPlayersNumber")
    void gamePlayable(int nPlayers) {
        var game = new Game();
        IntStream.range(0, nPlayers).forEach(n -> game.add("player "+n));
        Assertions.assertTrue(game.isPlayable());
    }

    @ParameterizedTest
    @MethodSource("invalidPlayersNumber")
    void gameNotPlayable(int nPlayers) {
        var game = new Game();
        IntStream.range(0, nPlayers).forEach(n -> game.add("player "+n));
        Assertions.assertFalse(game.isPlayable());
    }

}